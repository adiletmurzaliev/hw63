<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use AppBundle\Entity\PhraseTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TranslatorController extends Controller
{
    /**
     * @Route("/", name="translator_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('phrase', TextType::class, ['label' => $this->get('translator')->trans('translator.form.phrase')])
            ->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('translator.form.send')])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $phrase = new Phrase();
            /** @var PhraseTranslation $phraseTranslation */
            $phraseTranslation = $phrase->translate($request->getSession()->get('_locale'));
            $phraseTranslation->setContent($data['phrase']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();

            $em->flush();
        }

        $phrases = $this->getDoctrine()->getRepository(Phrase::class)->findAll();

        return $this->render('@App/Translator/index.html.twig', [
            'form' => $form->createView(),
            'phrases' => $phrases
        ]);
    }

    /**
     * @Route("/change_language/{_locale}", name="translator_change_language")
     *
     * @param Request $request
     * @return Response
     */
    public function changeLanguageAction(Request $request): Response
    {
        switch ($request->attributes->get('_locale')) {
            case 'ru':
                $request->getSession()->set('_locale', 'ru');
                break;
            case 'en':
                $request->getSession()->set('_locale', 'en');
                break;
        }

        return $this->redirectToRoute('translator_index');
    }

    /**
     * @Route("/edit/{id}", requirements={"id" : "\d+"}, name="translator_edit")
     *
     * @param Request $request
     * @param Phrase $phrase
     * @return Response
     */
    public function editPhraseAction(Request $request, Phrase $phrase): Response
    {
        $form = $this->createFormBuilder()->getForm();

        $languages = ['en', 'ru', 'es', 'fr', 'ja'];
        foreach ($languages as $lang){
            $content = $phrase->translate($lang, false)->getContent();
            if($content === null){
                $form->add($lang, TextType::class, ['label' => $lang]);
            } else{
                $form->add($lang, TextType::class, ['label' => $lang, 'data' => $content]);
            }
        }

        $form->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('translator.form.send')]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            foreach ($data as $lang => $value){
                $phrase->translate($lang, false)->setContent($value);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();

            $em->flush();

            return $this->redirectToRoute('translator_edit', ['id' => $phrase->getId()]);
        }

        return $this->render('@App/Translator/edit.html.twig', [
            'phrase' => $phrase,
            'form' => $form->createView(),
            'languages' => $languages
        ]);
    }

}
