<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhrasesData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $phrase = new Phrase();
        $phrase->translate('en')->setContent('First sentence');
        $phrase->translate('ru')->setContent('Первое предложение');
        $phrase->translate('es')->setContent('Primera oracion');
        $phrase->translate('fr')->setContent('Première phrase');
        $phrase->translate('ja')->setContent('最初の文章');

        $manager->persist($phrase);
        $phrase->mergeNewTranslations();
        $manager->flush();

        $phrase = new Phrase();
        $phrase->translate('en')->setContent('Second sentence');
        $phrase->translate('ru')->setContent('Второе предложение');

        $manager->persist($phrase);
        $phrase->mergeNewTranslations();
        $manager->flush();

        $phrase = new Phrase();
        $phrase->translate('ru')->setContent('Третья фраза');

        $manager->persist($phrase);
        $phrase->mergeNewTranslations();
        $manager->flush();
    }
}
