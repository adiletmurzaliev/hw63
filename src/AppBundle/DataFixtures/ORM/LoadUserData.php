<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('user')
            ->setPlainPassword('123')
            ->setEmail('User123@mail.com')
            ->setEnabled(1);

        $manager->persist($user);
        $manager->flush();
    }
}
